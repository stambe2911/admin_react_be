<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('posts', 'PostsC');

Route::post('/uploadPostImages', 'PostsC@uploadPostImages');

Route::post('/editPostImages', 'PostsC@editPostImages');

Route::post('/api/addOrRemovePost', 'PostsC@addOrRemove');

Route::post('/verifyAdmin', 'AdminC@verifyAdmin');

Route::get('/getStatistics', 'AdminC@getStatistics');

Route::get('/getCategories', 'PostsC@getCategories');

Route::get('/getDepartments', 'PostsC@getDepartments');

Route::get('/getCities', 'PostsC@getCities');

Route::post('/api/uploadEvent', 'EventsC@uploadEvent');

Route::post('/api/getEvents', 'EventsC@getEvents');