<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class AdminC extends Controller
{
    function verifyAdmin(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');

        $verify = DB::select('select * from admin where username = ? and password = ?', [$username, $password]);

        return json_encode($verify);
    }

    function getStatistics(){
        $statistics = DB::select('select ( select count(*) from posts where status = 1 ) AS activePosts, ( select count(*) from events where status = 1 ) AS activeEvents, ( select count(*) from users where status = 1 ) AS activeUsers FROM dual');

        return '{ "activePosts" : "'.$statistics[0]->activePosts.'" , "activeEvents" : "'.$statistics[0]->activeEvents.'" , "activeUsers" : "'.$statistics[0]->activeUsers.'" }';
    }
}
