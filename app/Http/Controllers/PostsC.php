<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class PostsC extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = DB::select('select * from posts order by postId desc');

        return json_encode($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->input('title');
        $description = $request->input('description');
        $categoryId = $request->input('category');
        $departments = $request->input('departments');
        $cities = $request->input('cities');
        $shareable = $request->input('shareable');
        $customerSpeakPost = $request->input('customerSpeakPost');
        $inquiryPost = $request->input('inquiryPost');

        $postId = DB::table('posts')->insertGetId(['withImages' => 0, 'title' => $title, 'description' => $description, 'categoryId' => $categoryId, 'departments' => $departments, 'cities' => $cities, 'shareable' => $shareable, 'customerSpeakPost' => $customerSpeakPost, 'inquiryPost' => $inquiryPost, 'status' => 1]);

        sleep(3);

        $insertedRecord = ["postId" => $postId, "title" => $title, "description" => $description, "status" => 1, "btnStatus" => [true, false], "editBtn" => $postId];

        $response = ["status" => "success", "message" => "Post Uploaded Successfully", "insertedRecord" => $insertedRecord];

        return json_encode($response); 
    }

    public function uploadPostImages(Request $request){
        $postId = $request->input('postId');
        $image = $request->file('image'); 
        
        $imageExtension = "";  
        if($image->getMimeType() == "image/png"){

            $imageExtension = "png";
        }else{  // "image/jpeg"
    
            $imageExtension = "jpg";
        }
        
        $newFilename = md5(uniqid()).mt_rand (0,99); 
        $newFile = $newFilename.".".$imageExtension; 
        
        $destination = public_path('/postImages');
        $flg = $image->move($destination, $newFile); 
        
        if($flg){
            DB::insert('insert into posts_images(postId, name, extnsn) values(?, ?, ?)', [$postId, $newFilename, $imageExtension]);

            DB::update('update posts set withImages = ? where postId = ?', [1, $postId]);
        }

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postId = $id;

        $postDetails = DB::select('select withImages, title, IFNULL(description, "") as description, categoryId, departments, cities, shareable, inquiryPost, customerSpeakPost from posts where postId = ?', [$postId]);   
        $postDetails = $postDetails[0];

        $postImages = [];
        if($postDetails->withImages){
            $postImages = DB::select('select name, extnsn from posts_images where postId = ?', [$postId]);
        }

        $postDetails->postImages = $postImages;


        $categories = DB::select('select * from categories');

        $departments = DB::select('select * from departments');

        $cities = DB::select('select * from cities');

        $response = ['postDetails' => $postDetails, 'categories' => $categories, 'departments' => $departments, 'cities' => $cities];

        sleep(2);

        return json_encode($response);
    }

    public function addOrRemove(Request $request){
        $postId = $request->input('postId');
        $status = $request->input('status');

        DB::update('update posts set status = ? where postId = ?', [$status, $postId]);

        return 1;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){ 
        $postId = $id; 
        $title = $request->input('title');  
        $description = $request->input('description');
        $categoryId = $request->input('category');
        $departments = $request->input('departments');
        $cities = $request->input('cities');
        $shareable = $request->input('shareable');
        $customerSpeakPost = $request->input('customerSpeakPost');
        $inquiryPost = $request->input('inquiryPost');

        DB::table('posts')->where('postId', $postId)->update(['title' => $title, 'description' => $description, 'categoryId' => $categoryId, 'departments' => $departments, 'cities' => $cities, 'shareable' => $shareable, 'customerSpeakPost' => $customerSpeakPost, 'inquiryPost' => $inquiryPost]);

        sleep(3);

        return '{ "status" : "success" , "message" : "Post Details have been updated" }'; 
    }

    public function editPostImages(Request $request){
        $postId = $request->input('postId');   
        $currentIndex = $request->input('currentIndex');
        $image = $request->file('image'); 
        
        $imageExtension = "";  
        if($image->getMimeType() == "image/png"){

            $imageExtension = "png";
        }else{  // "image/jpeg"
    
            $imageExtension = "jpg";
        }
        
        $newFilename = md5(uniqid()).mt_rand (0,99); 
        $newFile = $newFilename.".".$imageExtension; 
        
        $destination = public_path('/postImages');
        $flg = $image->move($destination, $newFile); 
        
        if($flg){
            if($currentIndex == 0){  // Before Inserting details of First Image in Database, Remove existing Images and also update 'withImages' of post.
                $existingImages = DB::select('select * from posts_images where postId = ?', [$postId]);

                foreach($existingImages as $key => $value){
                    $fullPath = public_path('/postImages/').$value->name.'.'.$value->extnsn;
                    $this->deleteFile($fullPath);
                }

                DB::delete('delete from posts_images where postId = ?', [$postId]);
                
                DB::update('update posts set withImages = ? where postId = ?', [1, $postId]); 
            }

            DB::insert('insert into posts_images(postId, name, extnsn) values(?, ?, ?)', [$postId, $newFilename, $imageExtension]);   
        }

        /* Code to Remove existing Images */

        return 1;
    }

    function deleteFile($sourcePath){
		
		if(@getimagesize($sourcePath)){

			@unlink($sourcePath);
		}	
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCategories(){
        $categories = DB::select('select * from categories');

        return json_encode($categories);
    }

    public function getDepartments(){
        $departments = DB::select('select * from departments');

        return json_encode($departments);
    }

    public function getCities(){
        $cities = DB::select('select * from cities');

        return json_encode($cities);
    }
}
