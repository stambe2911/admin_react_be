<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class EventsC extends Controller
{
    function uploadEvent(Request $request){
        $eventTitle = $request->input('eventTitle');
        $eventTime = $request->input('eventTime');

       $eventId = DB::table('events')->insertGetId(['eventTitle' => $eventTitle, 'eventTime' => $eventTime, 'status' => 1]);

        sleep(3);

        $insertedRecord = ['eventId' => $eventId, 'eventTitle' => $eventTitle, 'eventTime' => $eventTime];

        $response = ["status" => "success", "message" => "Event Uploaded Successfully", "insertedRecord" => $insertedRecord];

        return json_encode($response); 
    }

    function getEvents(Request $request){
        $offset = $request->input('pageNo');
        $limit = 5;   
        
        if($offset == 1){ 
            $offset = $offset - 1;  
            
        }else{
            $offset = $offset - 1;  
            $offset = $offset * $limit;
        }

        $events = DB::select('select * from events order by eventId desc limit ? offset ?', [$limit, $offset]);

        sleep(3);

        return json_encode($events);
    }
}
